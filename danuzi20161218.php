<?php

require_once 'pear/function.php';

/** settings **/
$images_dir = 'img/20161218/gallery/';
$thumbs_dir = 'img/20161218/thumbs/';
$thumbs_width = 340;
$images_per_row = 3;

/** generate photo gallery **/
$image_files = get_files($images_dir);
$gallery_str = '';
if (count($image_files)) {
    $index = 0;
    foreach ($image_files as $index => $file) {
        ++$index;
        $thumbnail_image = $thumbs_dir.$file;
        if (!file_exists($thumbnail_image)) {
            $extension = get_file_extension($thumbnail_image);
            if ($extension) {
                make_thumb($images_dir.$file, $thumbnail_image, $thumbs_width);
            }
        }
        $gallery_str .= '<div class="photo_thumb"><a href="'.$images_dir.$file.'" data-lightbox="roadtrip" class="photo-link smoothbox" rel="gallery"><img class="resize_fit_center" src="'.$thumbnail_image.'" /></a></div>';
        if ($index % $images_per_row == 0) {
            $gallery_str .= '<div class="clear"></div>';
        }
    }
    $gallery_str .= '<div class="clear"></div>';
} else {
    $gallery_str .= '<p>There are no images in this gallery.</p>';
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Domestique.tw - cycling in Taiwan, Taiwan bike tour. Customize your trip.</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <link href="css/lightbox.css" rel="stylesheet">
</head>

<body id="page-top" class="index">
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-88662275-1', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <a class="navbar-brand page-scroll" href="#page-top">Domestique.tw</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header id="danuzi">
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in"></div>
                <div class="intro-heading"></div>
                <div class="intro-lead-hit"></div>
            </div>
        </div>
    </header>

    <!-- Services Section -->
    <section id="gallery">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"></h2>
                </div>
            </div>
            <div class="row">
                <?php echo $gallery_str; ?>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Domestique.tw 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://www.facebook.com/domestique.tw/"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://www.instagram.com/domestique.tw/"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">

                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/agency.min.js"></script>

    <script src="js/lightbox.js"></script>
</body>

</html>
